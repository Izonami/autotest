package com.lol.browserprovider;

import com.codeborne.selenide.Configuration;
import com.lol.browserprovider.chrome.ChromeBrowserProvider;
import com.lol.browserprovider.chrome.ChromeLocalBrowserProvider;
import lombok.extern.slf4j.Slf4j;

/**
 * Фабрика для загрузки нужного браузера в зависимости от данных в конфигах {@link com.lol.config.BaseConfig}
 */
@Slf4j
public final class DriverProviderFactory {

    public static void createInstance(final String browserName) {
        switch (browserName) {
            case "Chrome": {
                log.info("Selected Remote Chrome driver");
                Configuration.browser = ChromeBrowserProvider.class.getName();
                break;
            }
            case "ChromeLocal": {
                log.info("Select Local Chrome driver");
                Configuration.browser = ChromeLocalBrowserProvider.class.getName();
                break;
            }
        }
    }
}
