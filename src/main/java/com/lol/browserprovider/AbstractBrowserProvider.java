package com.lol.browserprovider;

import com.codeborne.selenide.WebDriverProvider;
import com.lol.config.BaseConfig;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URI;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

/**
 * Абстрактный класс скрывающий настройки подключения к удалённому\локальному драйверу,
 * настройки для запуска браузера и уровни логирования ошибок.
 * Наследует {@link WebDriverProvider} что бы наследники обязательно реализовали его метод
 * необходимый для корректного запуска браузера
 */
@Slf4j
public abstract class AbstractBrowserProvider implements WebDriverProvider {

    protected @Getter WebDriver driver;

    /**
     * Запуск {@link RemoteWebDriver}
     * @param capabilities
     */
    protected void createRemoteDriver(final @NonNull DesiredCapabilities capabilities) {
        try {
            driver = new RemoteWebDriver(
                    URI.create(BaseConfig.REMOTE_URL).toURL(),
                    capabilities);
            driver.manage().window().setSize(new Dimension(BaseConfig.BROWSER_WIDTH, BaseConfig.BROWSER_HEIGHT));
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            ((RemoteWebDriver)driver).setFileDetector(new LocalFileDetector());
        } catch (MalformedURLException e) {
            log.error("Protocol exception ", e);
        }
    }

    /**
     * Запуск локального драйвера
     * @param desiredCapabilities
     */
    protected void createLocalChromeDriver(final @NonNull DesiredCapabilities desiredCapabilities) {
        driver = new ChromeDriver(desiredCapabilities);
    }

    /**
     * Опции запуска хрома
     * @return
     */
    protected ChromeOptions chromeOptions() {
        final ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");
        options.addArguments("--disable-infobars");
        options.addArguments("--disable-bundled-ppapi-flash");

        return options;
    }

    protected ChromeOptions chromeOptionsLocal() {
        final ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-infobars");
        options.addArguments("--disable-bundled-ppapi-flash");

        return options;
    }

    /**
     * Уровни логирования
     * @return
     */
    protected LoggingPreferences getLogPref() {
        final LoggingPreferences logs = new LoggingPreferences();
        logs.enable(LogType.BROWSER, Level.SEVERE);
        logs.enable(LogType.CLIENT, Level.OFF);
        logs.enable(LogType.DRIVER, Level.WARNING);
        logs.enable(LogType.PERFORMANCE, Level.INFO);
        logs.enable(LogType.SERVER, Level.ALL);

        return logs;
    }
}
