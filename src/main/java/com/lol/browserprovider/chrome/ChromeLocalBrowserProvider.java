package com.lol.browserprovider.chrome;

import com.lol.browserprovider.AbstractBrowserProvider;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.DriverManagerType;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

@Slf4j
public final class ChromeLocalBrowserProvider extends AbstractBrowserProvider {

    @Override
    public WebDriver createDriver(DesiredCapabilities desiredCapabilities) {
        ChromeDriverManager.getInstance(DriverManagerType.CHROME).setup();

        desiredCapabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptionsLocal());
        desiredCapabilities.setCapability(CapabilityType.LOGGING_PREFS, getLogPref());

        createLocalChromeDriver(desiredCapabilities);

        return driver;
    }
}
