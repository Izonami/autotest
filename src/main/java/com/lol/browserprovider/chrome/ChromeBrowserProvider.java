package com.lol.browserprovider.chrome;

import com.lol.browserprovider.AbstractBrowserProvider;
import com.lol.config.BaseConfig;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

public final class ChromeBrowserProvider extends AbstractBrowserProvider {

    @Override
    public WebDriver createDriver(DesiredCapabilities desiredCapabilities) {
        desiredCapabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions());
        desiredCapabilities.setCapability(CapabilityType.LOGGING_PREFS, getLogPref());
        desiredCapabilities.setBrowserName("chrome");
        desiredCapabilities.setVersion(BaseConfig.BROWSER_VERSION);
        desiredCapabilities.setCapability("screenResolution", BaseConfig.BROWSER_WIDTH+"x"+BaseConfig.BROWSER_HEIGHT+"x24");

        createRemoteDriver(desiredCapabilities);

        return driver;
    }
}
