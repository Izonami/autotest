package com.lol.helper;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import lombok.extern.slf4j.Slf4j;

import static com.codeborne.selenide.Selenide.$;
import static org.openqa.selenium.By.xpath;

/**
 * Класс обёртка для методов Selenide. Проверяет переданные значения на пустоту и задаёт {@link Condition} по умолчанию
 * Цель - минимизировать написание кода и получить на выходе более понятные названия переменных
 */
@Slf4j
public final class ElementHelper {

    private ElementHelper() {}

    /**
     * Выкидывает exception если не увидел элемент
     * @param xpath
     * @return если элемент отображается true, если нет false
     */
    public static boolean isDisplayed(final String xpath) {
        if (xpath.trim().isEmpty()) {
            log.error("In method \"isDisplayed\" something wrong. xpath selector is empty");
            throw new NullPointerException();
        }
        return $(xpath(xpath)).isDisplayed();
    }

    /**
     * Заполняет поле данными
     * @param xpath
     * @param value
     */
    public static void fillField(final String xpath, final String value) {
        if (xpath.trim().isEmpty()) {
            log.error("In method \"fillField\" something wrong. xpath selector or value is empty");
            throw new NullPointerException();
        }
        $(xpath(xpath)).shouldBe(Condition.visible).setValue(value);
    }

    /**
     * Эмулирует клик в поле, кнопку, объект
     * @param xpath
     */
    public static void clickIn(final String xpath) {
        if (xpath.trim().isEmpty()) {
            log.error("In method \"clickIn\" something wrong. Selector is empty");
            throw new NullPointerException();
        }
        $(xpath(xpath)).shouldBe(Condition.visible).click();
    }

    /**
     * Выполняет переданный js код
     * @param jsCode
     * @return
     */
    public static String getExecuteJs(final String jsCode){
        log.info("Execute JS code {}", jsCode);
        return Selenide.executeJavaScript(jsCode);
    }
}
