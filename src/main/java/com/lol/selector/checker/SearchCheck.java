package com.lol.selector.checker;

import com.lol.helper.ElementHelper;
import io.qameta.allure.Step;
import org.testng.Assert;

public interface SearchCheck {

    @Step("Проверка нахождения на странице поиска")
    default void checkSearchPage(final String errorMsg) {
        Assert.assertTrue(ElementHelper.isDisplayed("//*[@id=\"result-stats\"]"), errorMsg);
    }
}
