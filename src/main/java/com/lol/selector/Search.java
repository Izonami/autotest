package com.lol.selector;

import com.lol.helper.ElementHelper;
import io.qameta.allure.Step;

public interface Search {

    @Step("Кликнуть в поле поиска и заполнить поле данными:{0}")
    default void clickAndFillField(final String value) {
        ElementHelper.fillField("//*[@id=\"tsf\"]/div[2]/div[1]/div[1]/div/div[2]/input", value);
    }

    @Step("Нажать на кнопку Поиск в Google")
    default void clickOnButton() {
        ElementHelper.clickIn("//*[@id=\"tsf\"]/div[2]/div[1]/div[3]/center/input[1]");
    }
}
