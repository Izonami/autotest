package com.lol.config;

import com.google.common.io.Resources;
import com.lol.config.core.ConfigEngine;

/**
 * Каждый класс передается загрузчику конфигурационных файлов в методе {@link ConfigManager#loadConfig()}
 */
public final class ConfigManager {

    static final String BASE_CONFIG_NAME = "base";
    static final String SITE_CONFIG_NAME = "site";

    private static final String CONFIG_DIR = ConfigManager.class.getClassLoader().getResource("config/").getPath();
    private static final ConfigEngine configEngine = new ConfigEngine();

    private ConfigManager(){}

    /**
     * Список конфугурационных файлов для загрузки
     */
    public static void loadConfig() {
        configEngine.loadConfig(BaseConfig.class, BASE_CONFIG_NAME, CONFIG_DIR);
        configEngine.loadConfig(SiteConfig.class, SITE_CONFIG_NAME, CONFIG_DIR);
    }
}
