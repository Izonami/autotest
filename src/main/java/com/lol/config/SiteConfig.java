package com.lol.config;

import com.lol.config.core.ConfigField;

import static com.lol.config.ConfigManager.SITE_CONFIG_NAME;

public final class SiteConfig {

    @ConfigField(config = SITE_CONFIG_NAME, fieldName = "url", value = "https://google.com")
    public static String URL;
}
