package com.lol.config.core;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.nio.charset.StandardCharsets;
import java.util.regex.Pattern;

@Slf4j
public final class ConfigEngine {

    private static final Pattern PATTERN = Pattern.compile("_([A-Za-z0-9])");

    /**
     * Считывает данные из файла и проверяет наличие в классе полей аннотированных {@link ConfigField}
     * после чего через рефлексию получает к ним доступ и задает значение в соответсвии с данными полученными
     * из конфигурационного файла, в случае если в конфигурационном файле, не была найдена пара ключ=значение
     * соответствующая данным из аннотации {@link ConfigField} значение для этой переменной задается из указанного
     * дефолтного значение {@link ConfigField#value()}
     * В случае если файл не был найден, выпадает {@link Exception}
     * В случае если не удалось распарсить значение из файла, бросается {@link NumberFormatException}
     * В качестве аргументов получает:
     * @param configClass - Класс в котором ищутся поля аннатированные {@link ConfigField}
     * @param config - имя конфигурационного файла
     * @param dir - дирректория с конфигурационными файлами
     */
    public void loadConfig(@NonNull final Class<?> configClass, final String config, final String dir) {
        try(val lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(dir+config + ".properties"), StandardCharsets.UTF_8))) {
            final ExProperties settings = new ExProperties();
            settings.load(lnr);

            for(val field : configClass.getDeclaredFields()) {
                ConfigField configField;
                if((configField = field.getAnnotation(ConfigField.class)) != null && config.equals(configField.config())) {
                    final String fld = field.getType().getSimpleName().toLowerCase();
                    String fieldName = configField.fieldName();

                    if(fieldName.isEmpty())
                        fieldName = getNormalName(field.getName());

                    try {
                        switch (fld) {
                            case "boolean" : field.setBoolean(null, settings.getBooleanProperty(fieldName, configField.value()));
                                break;
                            case "byte" : field.setByte(null, settings.getByteProperty(fieldName, configField.value()));
                                break;
                            case "int" : field.setInt(null, settings.getIntProperty(fieldName, configField.value()));
                                break;
                            case "long" : field.setLong(null, settings.getLongProperty(fieldName, configField.value()));
                                break;
                            case "float" : field.setFloat(null, settings.getFloatProperty(fieldName, configField.value()));
                                break;
                            case "double" : field.setDouble(null, settings.getDoubleProperty(fieldName, configField.value()));
                                break;
                            case "string" : field.set(null, settings.getProperty(fieldName, configField.value()));
                                break;
                            default : log.info("Unknown field type: " + field.getType().getSimpleName() + " field name: " + field.getName() + " config: " + config + ".properties");
                                break;
                        }
                    }
                    catch(NumberFormatException e) {
                        log.error("Failed to Load config/" + dir + " " + config + ".properties file. Field: " + field.getName() + " " + e.getMessage());
                    }
                    log.debug(config + ": set " + field.getName() + "{" + fieldName + "} = " + field.get(null));
                }
            }
        }
        catch(Exception e) {
            log.error("Failed to Load config/" + dir + " " + config + ".properties file.", e);
        }
    }

    /**
     * Если небыло указанно значение для {@link ConfigField#fieldName()}
     * пытаемся найти подходящий ключ в конфигурационном файле по названию поля.
     * @param name
     * @return
     */
    private String getNormalName(String name) {
        String normalName = name.toLowerCase();
        val sb = new StringBuilder();
        val m = PATTERN.matcher(normalName);

        while(m.find()) {
            m.appendReplacement(new StringBuffer(sb), m.group(0).replace("_" + m.group(1), m.group(1).toUpperCase()));
        }

        m.appendTail(new StringBuffer(sb));
        return sb.replace(0, 1, sb.toString().substring(0, 1).toUpperCase()).toString();
    }
}
