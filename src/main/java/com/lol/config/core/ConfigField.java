package com.lol.config.core;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Аннотация для полей классов конфигурационных фаилов
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ConfigField {

    /**
     * Имя конфигурационного файла к которому относится пара ключ=значение
     * @return
     */
    String config();

    /**
     * Название ключа
     * @return
     */
    String fieldName() default "";

    /**
     * Определяет значение по умолчанию, в случае если по каким либо причинам,
     * не удалось получить значение из конфигурационного файла.
     * Может задать значение даже если такой пары ключ=значение, не существует в
     * конфигурационном файле.
     * @return
     */
    String value() default "";
}
