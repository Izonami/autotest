package com.lol.config;

import com.lol.config.core.ConfigField;

import static com.lol.config.ConfigManager.BASE_CONFIG_NAME;

public final class BaseConfig {

    @ConfigField(config = BASE_CONFIG_NAME, fieldName = "remoteUrl", value = "http://localhost:4444/wd/hub")
    public static String REMOTE_URL;
    @ConfigField(config = BASE_CONFIG_NAME, fieldName = "browserType", value = "Chrome")
    public static String BROWSER_TYPE;
    @ConfigField(config = BASE_CONFIG_NAME, fieldName = "browserVersion", value = "58.0")
    public static String BROWSER_VERSION;
    @ConfigField(config = BASE_CONFIG_NAME, fieldName = "browserWidth", value = "800")
    public static int BROWSER_WIDTH;
    @ConfigField(config = BASE_CONFIG_NAME, fieldName = "browserHeight", value = "600")
    public static int BROWSER_HEIGHT;
    @ConfigField(config = BASE_CONFIG_NAME, fieldName = "maxWindow", value = "true")
    public static boolean MAX_WINDOW;
}
