package com.lol.report;

import org.testng.ITestResult;

public final class AllureRetryAnalyzer extends AbstractAllureRetryAnalyzer {

    private static final int MAX_RETRY_COUNT = 2;

    private int retryCount;

    @Override
    public boolean retry(ITestResult result, boolean getRetryAbilityOnly) {
        final RetryFailed annotation = result.getMethod().getConstructorOrMethod().getMethod().getAnnotation(RetryFailed.class);
        final int customRetryCount = annotation != null ? annotation.count():0;
        if (!result.isSuccess()) {
            if (retryCount < (customRetryCount > 0 ? customRetryCount : MAX_RETRY_COUNT)) {
                if (!getRetryAbilityOnly)
                    retryCount++;
                result.setStatus(ITestResult.SUCCESS);
                return true;
            } else {
                result.setStatus(ITestResult.FAILURE);
            }
        }
        return false;
    }
}
