package com.lol.report;

import org.testng.ITestResult;

public abstract class AbstractAllureRetryAnalyzer implements IAllureRetryAnalyzer {

    @Override
    public boolean retry(ITestResult result) {
        return retry(result, false);
    }
}
