package com.lol.report;

import com.codeborne.selenide.WebDriverRunner;
import com.codeborne.selenide.logevents.EventsCollector;
import com.codeborne.selenide.logevents.LogEvent;
import com.codeborne.selenide.logevents.SelenideLogger;
import com.google.common.base.Joiner;
import io.qameta.allure.Attachment;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.Logs;

import java.util.Collections;
import java.util.Objects;
import java.util.OptionalInt;

@Slf4j
public final class CustomReport {

    public void start() {
        SelenideLogger.addListener("simpleReport", new EventsCollector());
    }

    public void finish(final String title) {
        final EventsCollector logEventListener = SelenideLogger.removeListener("simpleReport");
        final OptionalInt maxLineLength = logEventListener
                .events()
                .stream()
                .filter(Objects::nonNull)
                .map(LogEvent::getElement)
                .map(String::length)
                .mapToInt(Integer::intValue)
                .max();
        final int count = maxLineLength.orElse(0) >= 20 ? maxLineLength.getAsInt() + 1:20;

        final StringBuilder sb = new StringBuilder();
        sb.append("Report for ").append(title).append('\n');

        final String delimiter = '+' + Joiner
                .on('+')
                .join(this.joinLine(count),
                        this.joinLine(70),
                        new Object[]{this.joinLine(10),
                                this.joinLine(10),
                                this.joinLine(100)}
                                ) + "+\n";
        sb.append(delimiter);
        sb.append(String.format("|%-" + count + "s|%-70s|%-10s|%-10s|%-100s|%n", "Element", "Subject", "Status", "ms.", "Error"));
        sb.append(delimiter);

        for (final LogEvent e : logEventListener.events()) {
            sb.append(String.format("|%-" + count + "s|%-70s|%-10s|%-10s|%-100s|%n", e.getElement(), e.getSubject(), e.getStatus(), e.getDuration(), getConsoleLog()));
        }

        sb.append(delimiter);
        log.info(sb.toString());
        attachText(sb.toString());
    }

    private String joinLine(final int count) {
        return Joiner.on("").join(Collections.nCopies(count, "-"));
    }

    private String getConsoleLog() {
        final StringBuilder sb = new StringBuilder();
        final Logs logs = WebDriverRunner.getWebDriver().manage().logs();
        final LogEntries logEntries = logs.get(LogType.BROWSER);

        for (final LogEntry logEntry : logEntries) {
            sb.append(logEntry.getMessage()).append("\n");
        }

        return sb.toString();
    }

    @Attachment(value = "Test log {0}")
    private String attachText(final String text) {
        return text;
    }
}
