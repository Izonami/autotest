package com.lol.report;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


/**
 * Позволяет устанавливать отдельное количество перезапусков
 * на конкретный метод. Реализация в {@link AllureRetryAnalyzer}
 * через рефлексию получает количество перезапусков заданное для метода.
 * {@code RetryFailed annotation = result.getMethod().getConstructorOrMethod().getMethod().getAnnotation(RetryFailed.class);}
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface RetryFailed {
    int count() default 0;
}
