package com.lol.report;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public interface IAllureRetryAnalyzer extends IRetryAnalyzer {

    boolean retry(ITestResult result, boolean getRetryAbilityOnly);
}
