package com.lol.listener;

import com.codeborne.selenide.WebDriverRunner;
import com.lol.helper.ElementHelper;
import com.lol.report.CustomReport;
import com.lol.report.IAllureRetryAnalyzer;
import io.qameta.allure.Allure;
import io.qameta.allure.AllureLifecycle;
import io.qameta.allure.Attachment;
import io.qameta.allure.model.Status;
import io.qameta.allure.model.StepResult;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;

import java.util.Iterator;
import java.util.UUID;

@Slf4j
public final class TestListenerImpl implements ITestListener {

    private final CustomReport report;
    private final AllureLifecycle allureLifecycle;

    public TestListenerImpl() {
        this.report = new CustomReport();
        this.allureLifecycle = Allure.getLifecycle();
    }

    @Override
    public void onTestStart(final ITestResult iTestResult) {
        log.info("Test started running:"  + iTestResult.getMethod().getMethodName() + " at:" + iTestResult.getStartMillis());
        this.report.start();
    }

    @Override
    public void onTestSuccess(final ITestResult iTestResult) {
        log.info("Result success\n");
        this.report.finish(iTestResult.getName());
        takeScreenshot(iTestResult.getName());
    }

    @Override
    public void onTestFailure(final ITestResult iTestResult) {
        log.info("Result failure:" + iTestResult.getName());
        takeScreenshot(iTestResult.getName());
        fireRetryTest("The test has been failed then retried.", iTestResult);
        this.report.finish(iTestResult.getName());
        attachText(ElementHelper.getExecuteJs("return document.getElementsByTagName('html')[0].innerHTML"));
    }

    @Override
    public void onTestSkipped(final ITestResult iTestResult) {
        log.info("Test skipped! " + iTestResult.getName());
        this.report.finish(iTestResult.getName());
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(final ITestResult iTestResult) {
        takeScreenshot(iTestResult.getName());
        fireRetryTest("The test has been failed (within success percentage) then retried.", iTestResult);
        this.report.finish(iTestResult.getName());
    }

    @Override
    public void onStart(final ITestContext iTestContext) {
        //Nothing to do
    }

    @Override
    public void onFinish(final ITestContext iTestContext) {
        cleanupSkippedTests(iTestContext);
    }

    @Attachment(value = "Debug log {0}")
    private String attachText(final String text) {
        return text;
    }

    @Attachment(value = "Screenshot of {0}", type = "image/jpg")
    private byte[] takeScreenshot(String name) {
        return ((TakesScreenshot) WebDriverRunner.getWebDriver()).getScreenshotAs(OutputType.BYTES);
    }

    private void fireRetryTest(String message, final ITestResult result) {
        if (((IAllureRetryAnalyzer) result.getMethod().getRetryAnalyzer()).retry(result, true)) {
            final String uuid = UUID.randomUUID().toString();
            allureLifecycle.startStep(uuid, new StepResult().withName(message).withStatus(Status.FAILED));
            allureLifecycle.stopStep(uuid);
        }
    }

    private void cleanupSkippedTests(final ITestContext iTestContext) {
        final Iterator<ITestResult> failedTestCases = iTestContext.getFailedTests().getAllResults().iterator();
        while (failedTestCases.hasNext()) {
            final ITestResult failedTestCase = failedTestCases.next();
            final ITestNGMethod method = failedTestCase.getMethod();
            if (iTestContext.getFailedTests().getResults(method).size() > 1) {
                failedTestCases.remove();
            } else {
                if (iTestContext.getPassedTests().getResults(method).size() > 0) {
                    failedTestCases.remove();
                }
            }
        }
    }
}
