package com.lol.listener;

import com.codeborne.selenide.Configuration;
import com.lol.browserprovider.DriverProviderFactory;
import com.lol.config.BaseConfig;
import com.lol.config.ConfigManager;
import lombok.extern.slf4j.Slf4j;
import org.testng.IExecutionListener;

@Slf4j
public final class ExecutionListenerImpl implements IExecutionListener {

    @Override
    public void onExecutionStart() {
        log.info("Loading config files");
        ConfigManager.loadConfig(); //Загружаем файлы конфигурации

        log.info("Configuration for selenide:");

        Configuration.timeout = 6000;
        Configuration.savePageSource = false;//Отключает создание html файлов страницы
        //Эксперементально, по идее должно ускорить выполнение тестов
        Configuration.fastSetValue = true;
        Configuration.browserSize = System.getProperty("selenide.browserSize",
                System.getProperty("selenide.browser-size",BaseConfig.BROWSER_WIDTH+"x"+BaseConfig.BROWSER_HEIGHT));
        Configuration.headless = true;

        DriverProviderFactory.createInstance(BaseConfig.BROWSER_TYPE);

        log.info("timeout: " + Configuration.timeout);
        log.info("savePageSource: " + Configuration.savePageSource);
        log.info("pageLoadStrategy: " + Configuration.pageLoadStrategy);
        log.info("reopenBrowserOnFail: " + Configuration.reopenBrowserOnFail);
        log.info("browserSize: " + Configuration.browserSize);

    }
}
