package com.lol.util;

import org.apache.commons.lang3.RandomStringUtils;

public final class Rnd {

    public static String rndString(final int count) {
        return RandomStringUtils.randomAlphabetic(count);
    }
}
