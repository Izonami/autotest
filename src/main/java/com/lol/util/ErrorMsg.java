package com.lol.util;

public final class ErrorMsg {

    public static final String MSG_ERR = "Ошибка";
    public static final String MSG_CANT_FIND_ELEMENT = "Не найден элемент";
}
