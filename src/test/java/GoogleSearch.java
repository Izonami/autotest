import com.lol.config.SiteConfig;
import com.lol.listener.ExecutionListenerImpl;
import com.lol.listener.TestListenerImpl;
import com.lol.report.AllureRetryAnalyzer;
import com.lol.report.RetryFailed;
import com.lol.selector.Search;
import com.lol.selector.checker.SearchCheck;
import com.lol.util.ErrorMsg;
import com.lol.util.Rnd;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;
import static org.testng.Assert.assertTrue;

@Feature("Google Search")
@Listeners({ExecutionListenerImpl.class, TestListenerImpl.class})
public final class GoogleSearch implements Search, SearchCheck {

    @Test(retryAnalyzer = AllureRetryAnalyzer.class)
    @RetryFailed(count = 1)
    @Story("Поиск в Google рандомной строки")
    public void testRandomSearchInGoogle() {
        open(SiteConfig.URL);
        clickAndFillField(Rnd.rndString(8));
        clickOnButton();

        checkSearchPage(ErrorMsg.MSG_CANT_FIND_ELEMENT);
    }
}
